
import 'package:flutter/material.dart';
import 'package:flutter_login_api/home/home.dart';
import 'package:flutter_login_api/login/login_screen.dart';
var routes = <String, WidgetBuilder>{
  "/LoginScreen": (BuildContext context) => Login(),
  "/HomeScreen": (BuildContext context) => HomeScreen(),

};
class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new MaterialApp(
      theme:
      ThemeData(primaryColor: Colors.orangeAccent,primarySwatch: Colors.orange,
          primaryColorDark: Colors.orangeAccent),
      debugShowCheckedModeBanner: false,
      home: Login(),
        routes: routes);
  }
}
