import 'package:flutter/material.dart';

class SecondScreen extends StatelessWidget {
  final String token;

  // receive data from the FirstScreen as a parameter
  SecondScreen({Key key, @required this.token}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Second screen')),
      body: Center(
        child: Text(token != null ? token : ''),
      ),
    );
  }
}
