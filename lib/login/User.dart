class Users {
  final String name;
  final String email;
  final String phonenumber;
  final String avatar;
  final String background_image;
  final String date_of_birth;
  final int point;
  final int currentRanking;

  Users(
      {this.name,
        this.email,
        this.phonenumber,
        this.avatar,
        this.background_image,
        this.date_of_birth,
        this.point,this.currentRanking});

  Users.fromJson(Map<String, dynamic> json)
      : this.name = json['name'],
        this.email = json['email'],
        this.phonenumber = json['phone_number'],
        this.avatar = json['avatar'],
        this.background_image = json['background_image'],
        this.point = json['point'],
        this.currentRanking = json['currentRanking'],
        this.date_of_birth = json['date_of_birth'];


  Map<String, dynamic> toJson() => {
    'name': this.name,
    'email': this.email,
    'phone_number': this.phonenumber,
    'avatar': this.avatar,
    'background_image': this.background_image,
    'date_of_birth': this.date_of_birth,
    'point': this.point,
    'currentRanking':this.currentRanking,
  };
}

