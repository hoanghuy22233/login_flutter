import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_login_api/home/home.dart';
import 'package:flutter_login_api/login/User.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'LoginViewModel.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  SharedPreferences sharedPreferences;
  bool _showpass = false;
  bool _load = false;
  // DatabaseHelper databaseHelper = new DatabaseHelper();
  Login_Server login = new Login_Server();
  String msgStatus = '';
  static final TextEditingController _emailController =
      new TextEditingController();
  final TextEditingController _passController = new TextEditingController();
  final String tokens;
  final Users iuser;
  _LoginState({this.iuser, this.tokens});



  @override
  Widget build(BuildContext context) {
    Widget loadingIndicator = _load
        ? new Container(
            color: Color(0xFFFDF4E5),
            width: 70.0,
            height: 70.0,
            child: new Padding(
                padding: const EdgeInsets.all(5.0),
                child: new Center(child: new CircularProgressIndicator())),
          )
        : new Container();
    // TODO: implement build
    return new WillPopScope(
      onWillPop: _onBackPressed,
      child:
      new Scaffold(
        body: Container(
          padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
          constraints: BoxConstraints.expand(),
          color: new Color(0xFFFDF4E5),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 60,
                ),
                Image.asset(
                  'assets/avatar.jpg',
                  width: 220.0,
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 20.0, top: 20.0),
                  child: StreamBuilder(
                      //stream: bloc.UserStream,
                      builder: (context, snapshot) => TextField(
                            style:
                                TextStyle(fontSize: 15, color: Colors.black38),
                            keyboardType: TextInputType
                                .emailAddress, // Use email input type for emails.
                            controller: _emailController,
                            decoration: InputDecoration(
                                contentPadding: EdgeInsets.only(left: 30.0),
                                filled: true,
                                fillColor: Color(0xffffffff),
                                labelText: "Số điện thoại hoặc Email",
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.orangeAccent, width: 1.0),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(40)),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.white, width: 1.0),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(40)),
                                ),
                                errorText:
                                    snapshot.hasError ? snapshot.error : null,
                                labelStyle: TextStyle(
                                    color: Color(0xffcac8cd), fontSize: 15.0)),
                          )),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10.0),
                  child: Stack(
                    alignment: AlignmentDirectional.centerEnd,
                    children: <Widget>[
                      StreamBuilder(
                        // stream: bloc.PassStream,
                        builder: (context, snapshot) => TextField(
                          style: TextStyle(fontSize: 15, color: Colors.black38),
                          controller: _passController,
                          obscureText: !_showpass,
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.only(left: 30.0),
                              filled: true,
                              fillColor: Color(0xffffffff),
                              labelText: "Mật khẩu",
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.orangeAccent, width: 1.0),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(40)),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.white, width: 1.0),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(40)),
                              ),
                              errorText:
                                  snapshot.hasError ? snapshot.error : null,
                              labelStyle: TextStyle(
                                  color: Color(0xffcac8cd), fontSize: 15.0)),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 20.0),
                        child: GestureDetector(
                            onTap: onToggleShowPass,
                            child: Icon(
                              _showpass
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Colors.orangeAccent,
                            )),
                      )
                    ],
                  ),
                ),
                Container(
                  constraints:
                      BoxConstraints.loose(Size(double.infinity, 45.0)),
                  alignment: AlignmentDirectional.centerEnd,
                  child: Padding(
                      padding: const EdgeInsets.only(bottom: 20.0),
                      child: Stack(
                        children: <Widget>[
                          GestureDetector(
                            onTap: onForgotpassClick,
                            child: Text(
                              "Quên mật khẩu?",
                              style: TextStyle(
                                  fontSize: 16, color: Color(0xff606470)),
                            ),
                          )
                        ],
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 20.0),
                  child: SizedBox(
                    width: double.infinity,
                    height: 56.0,
                    child: RaisedButton(
                        color: Color(0xfff3c168),
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(40.0))),
                        onPressed: onSignClick,
                        child: Text(
                          "Đăng nhập",
                          style:
                              TextStyle(color: Color(0xff474747), fontSize: 15),
                        )),
                  ),
                ),
                new Align(
                  child: loadingIndicator,
                  alignment: FractionalOffset.center,
                ),
                Container(
                  constraints:
                      BoxConstraints.loose(Size(double.infinity, 45.0)),
                  alignment: AlignmentDirectional.center,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 20.0),
                    child: Text(
                      "Hoặc",
                      style: TextStyle(fontSize: 16, color: Color(0xff606470)),
                    ),
                  ),
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 10,
                            ),
                            GestureDetector(
                              onTap: null,
                              child: Image.asset(
                                'assets/facebooka.png',
                                width: 40.0,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 5),
                              child: Text("Facebook"),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 10,
                            ),
                            GestureDetector(
                              onTap: null,
                              child:
                                  Image.asset('assets/google.png', width: 40.0),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 5),
                              child: Text("Google"),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 40.0, bottom: 30.0),
                  child: RichText(
                    text: TextSpan(
                        text: "Chưa có tài khoản? ",
                        style:
                            TextStyle(color: Color(0xff606470), fontSize: 16),
                        children: <TextSpan>[
                          TextSpan(
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  // Navigator.push(
                                  //     context,
                                  //     MaterialPageRoute(
                                  //         builder: (context) =>
                                  //             RegistrationScreen()));
                                },
                              text: "Đăng ký",
                              style: TextStyle(
                                color: Color(0xfff3c168),
                                fontSize: 16,
                                decoration: TextDecoration.underline,
                              ))
                        ]),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> onSignClick() async {
   String token = login.token;
   String kiemtra = "login";
   sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      if (_emailController.text.trim().toLowerCase().isNotEmpty &&
          _passController.text.trim().isNotEmpty) {
        setState(() => _load = true);
        login
            .loginData(_emailController.text.trim().toLowerCase(),
                _passController.text.trim())
            .whenComplete(() {
//          new Center(child: new CircularProgressIndicator(),);
          if (!login.status) {
            _showDialog();

            msgStatus = 'Check email or password';
          } else {
            setState(() => _load = false);
            sharedPreferences.get("token");
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => HomeScreen(
              )));

          }
        });
      }
    });
  }

  void onForgotpassClick() {
    setState(() {
      // Navigator.push(context,
      //     MaterialPageRoute(builder: (context) => Forgot_Pass_Screen()));
    });
  }

  void onToggleShowPass() {
    setState(() {
      _showpass = !_showpass;
    });
  }



  Widget roundedButton(String buttonLabel, Color bgColor, Color textColor) {
    var loginBtn = new Container(
      padding: EdgeInsets.all(5.0),
      alignment: FractionalOffset.center,
      decoration: new BoxDecoration(
        color: bgColor,
        borderRadius: new BorderRadius.all(const Radius.circular(10.0)),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: const Color(0xFF696969),
            offset: Offset(1.0, 6.0),
            blurRadius: 0.001,
          ),
        ],
      ),
      child: Text(
        buttonLabel,
        style: new TextStyle(
            color: textColor, fontSize: 20.0, fontWeight: FontWeight.bold),
      ),
    );
    return loginBtn;
  }

  Future<bool> _onBackPressed() {
    return showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
            content: new Text('Bạn chắc chắn muốn thoát ứng dụng!'),
            actions: <Widget>[
              new GestureDetector(
                onTap: () => Navigator.of(context).pop(false),
                child: roundedButton(
                    "Hủy", const Color(0xfff3c168), const Color(0xFFFFFFFF)),
              ),
              new GestureDetector(
                onTap: () => Navigator.of(context).pop(true),
                child: roundedButton(
                    "Đồng ý", const Color(0xfff3c168), const Color(0xFFFFFFFF)),
              ),
            ],
          ),
        ) ??
        false;
  }

  void _showDialog() {
    showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
            content: new Text('Kiểm tra tài khoản và mật khẩu của bạn!'),
            actions: <Widget>[
              Center(
                child: new GestureDetector(
                  onTap: () => Navigator.of(context).pop(true),
                  child: roundedButton("Đồng ý", const Color(0xfff3c168),
                      const Color(0xFFFFFFFF)),
                ),
              ),
            ],
          ),
        ) ??
        false;
  }
}
